var express = require('express');
var router = express.Router();
var History = require('../models/history');
var Template = require('../models/template');
var mongoogse = require('mongoose');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Image Processing' });
});
router.get('/partials/:name', function (req, res) {
    res.render('partials/' + req.params.name);
});

//get history
router.get('/api/histories', function(req, res){
  console.log('history get test');
  getHistory(res);
});

//select history
router.get('/api/histories/:_id', function(req,res){
  History.findOne({
    _id: req.params._id
  }, function (err, history){
    if(err)
      res.send(err);
    else
      res.json(history);
  });
});

//update history
router.post('/api/histories/:_id', function(req, res){
  History.findOneAndUpdate({
    _id: req.params._id
  }, req.body, function(err, history){
    if(err)
      res.send(err);
    else
      res.json(history);
  });
});

//create history
router.post('/api/histories', function(req, res){
  History.create(req.body, function(err, work){
    if(err)
      res.send(err);
    getHistory(res);
  });
});

//delete history
router.delete('/api/histories/:_id', function(req, res){
  History.remove({
    _id: req.params._id
  }, function(err, history){
    if(err)
      res.send(err);
    getHistory(res);
  });
});

function getHistory(res){
  History.find(function(err, history){
    if(err){
      res.send(err);
    }
    res.json(history);
  });
};



//get template
router.get('/api/templates', function(req, res){
  console.log('template get test');
  getTemplate(res);
});

//select template
router.get('/api/templates/:_id', function(req,res){
  Template.findOne({
    _id: req.params._id
  }, function (err, history){
    if(err)
      res.send(err);
    else
      res.json(history);
  });
});

//update template
router. post('/api/templates/:_id', function(req, res){
  Template.findOneAndUpdate({
    _id: req.params._id
  }, req.body, function(err, history){
    if(err)
      res.send(err);
    else
      res.json(history);
  });
});

//create template
router.post('/api/templates', function(req, res){
  console.log('createTemplates');
  Template.create(req.body, function(err, work){
    if(err)
      res.send(err);
    getTemplate(res);
  });
});

//delete template
router.delete('/api/templates/:_id', function(req, res){
  Template.remove({
    _id: req.params._id
  }, function(err, history){
    if(err)
      res.send(err);
    getTemplate(res);
  });
});

function getTemplate(res){
  Template.find(function(err, history){
    if(err){
      res.send(err);
    }
    res.json(history);
  });
};


//addHistory
router.post('/api/templates/add/history/:_id', function (req,res){
  Template.findOne({
    _id: req.params._id
  }, function (err, template){
    if(err){
      res.send(err);
    }else{
      template.histories.push(req.body);
      template.save(function (err){
        if (err)
          res.send(err);
          
        getTemplate(res);
      });
    }
  });
});

//removeHistory
router.post('/api/templates/delete/history/:_id', function(req, res){
  Template.findOne({
    _id: req.params._id
  }, function(err, template){
    if(err)
      res.send(err);
    else{
      template.histories.remove({
        _id: req.body.idx
      });
      template.save(function(err){
        if(err)
          res.send(err);
        getTemplate(res);
      });
    }
  });
});

//updateHistory
router.post('/api/templates/update/history/:_id', function(req, res){
  Template.findOne({
    _id: req.params._id
  }, function (err, template){
    if(err){
      res.send(err);
    } else{
      template.histories.remove({
        _id: req.body.idx
      });
      template.histories.push({
        _id: req.body.idx,
        title: req.body.task.title,
        min: req.body.task.min,
        max: req.body.task.max,
        step: req.body.task.step,
        value: req.body.task.value,
        value2: req.body.task.value2,
        order: req.body.task.order
      });
      template.save(function (err){
        if(err)
          res.send(err);
        console.log('update tem history success.');
        getTemplate(res);
      });
    }
  });
});

module.exports = router;
