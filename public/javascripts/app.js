var app = angular.module('imgProcessing', ['ngRoute', 'imgProcessing.controller']);
app.config(function ($routeProvider, $locationProvider){
	return $routeProvider
		.when('/', {
			templateUrl: 'partials/main.jade',
			controller: 'imgProCtrl'
		})
		.otherwise({
			redirectTo: '/'
		});
});