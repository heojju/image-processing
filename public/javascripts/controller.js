'use strict';
angular.module('imgProcessing.controller', ['ui.bootstrap','Service'])
	.controller('imgProCtrl', function ($scope, $http, $log, $modal, Templates){
		
		getHistory();
		getTemplate();
		$scope.filterDoubleRange = false;
		$scope.parameterDoubleRange = false;
		$scope.templateDoubleRange = false;
		$scope.tempIndex='';
		$scope.spaceCnt = 0;
		
		//history
		$scope.histories = new Array();
		
		//GET
		function getHistory(){
			$scope.histories = $scope.histories;
		}
		function getTemplate(){
			Templates.get().success(function (template){
				$scope.templates = template;
			});
		}
		
		//CREATE
		function createHistory(data){
			$scope.histories.push(data);
			getHistory();
		}
		
		//DELETE
		$scope.deleteHistory = function(idx){
			var modalInstance = $modal.open({
	            templateUrl: '/partials/deleteFormModal.jade',
	            controller: 'deleteCtrl'
	        });
			modalInstance.result.then(function(result){
				if(result){
					for(var i=0; i<$scope.histories.length; i++){
						if($scope.histories[idx].order < $scope.histories[i].order){
							$scope.histories[i].order = $scope.histories[i].order-1;
						}
					}
					$scope.histories.splice(idx,1);
				}
			});
		};
		
		$scope.deleteAllHistory = function(){
			var modalInstance = $modal.open({
	            templateUrl: '/partials/deleteFormModal.jade',
	            controller: 'deleteCtrl'
	        });
			modalInstance.result.then(function(result){
				if(result){
					$scope.histories.splice(0,$scope.histories.length);
				}
			});
		};
		$scope.deleteTemplate = function(id_del){
			var modalInstance = $modal.open({
	            templateUrl: '/partials/deleteFormModal.jade',
	            controller: 'deleteCtrl'
	        });
			modalInstance.result.then(function(result){
				if(result){
					Templates.delete(id_del).success(function(data){
						$scope.templates = data;
						$('#editer-bar').css('left','320px');
						setTimeout(function(){
							$('#editer-bar').css('display','none');
						}, 500);
					});
				}
			});
		};
		
		//UPDATE
		$scope.updateHistory = function(){
			var idx = $scope.editHistoryIdx;
			var range = $('#parameterRange')[0];
			var range2 = $('#parameterRange2')[0];
			
			$scope.histories[idx].min = range.min,
			$scope.histories[idx].max = range.max,
			$scope.histories[idx].step = range.step,
			$scope.histories[idx].value = range.value,
			$scope.histories[idx].value2 = range2 ? range2.value : 0;
			
			$scope.editHistory = $scope.histories[idx];
		};
		
		$scope.backHistory = function(){
			console.log('d');
			$scope.editHistory = $scope.histories[$scope.editHistoryIdx];
		};
		
		$scope.openCreateModal = function(){
			var modalInstance = $modal.open({
	            templateUrl: '/partials/createTemplateModal.jade',
	            controller: 'createTemplateCtrl',
				resolve: {
					histories: function(){
						return $scope.histories;
					}
				}
	        });
			modalInstance.result.then(function(result){
				if(result){
					$scope.templates = result;
					$scope.goTemplate();
				}
			});
		};
		
		//filter
		$scope.filters = [
			{id:0,title: "Median", cls:'median', min:0, max:10, step:1, value:0},
			{id:1,title: "Average", cls:'average', min:0, max:10, step:1, value:0},
			{id:2,title: "Blur(gaussian)", cls:'blur', min:0, max:20, step:1, value:0},
			{id:3,title: "Anisotropic diffusion", cls:'AD', min:0, max:120, step:1, value:0},
			{id:4,title: "Bilateral", cls:'bilateral', min:0, max:20, step:1, value:0, value2:0},
			{id:5,title: "Sharpen", cls:'sharpen', min:0, max:20, step:1, value:0},
			{id:6,title: "Unsharpen", cls:'unsharpen', min:0, max:20, step:1, value:0},
			{id:7,title: "High boost", cls:'HB', min:0, max:20, step:1, value:0, value2:0},
			{id:8,title: "Emboss", cls:'emboss', min:0, max:20, step:1, value:0},
			{id:9,title: "Brightness", cls:'brightness', min:0, max:20, step:1, value:0},
			{id:10,title: "Contrast", cls:'contrast', min:0, max:20, step:1, value:0},
			{id:11,title: "Gamma", cls:'gamma', min:0, max:20, step:1, value:0}
		];
		
		var height1 = '115px';
		var height2 = '200px';		
		
		var agt = navigator.userAgent.toLowerCase();
		if(agt.indexOf('firefox') != -1){
			height2 = '145px';
		}
		
		$scope.templateOpen = function(idx){
			if(idx==$scope.idx){
				$scope.idx='123';
				$('.tem'+idx+' #template-item').css('background-color','');
				$('#editer-bar').css('left','320px');
				setTimeout(function() {
					$('#editer-bar').css('display', 'none');
				}, 500);
			}else{
				$scope.spaceCnt = 0;
				$scope.nowTemplate = $scope.templates[idx];
				$scope.idx = idx;
				$scope.tempIndex = idx;
				$('#editer-bar').css('display', 'block');
				setTimeout(function() {
					$('#editer-bar').css('left','0');
				}, 100);
				//background change
				for(var i=0; i<$scope.templates.length; i++){
					$('.tem'+i+' #template-item').css('background-color','');
				}
				$('.tem'+idx+' #template-item').css('background-color','#dedede');
				setTimeout(function(){
					for(var i=0; i<$scope.nowTemplate.histories.length; i++){
						if($scope.nowTemplate.histories[i].title == 'Bilateral'){
							$('.temDoubleRange'+$scope.nowTemplate.histories[i].order).css('display','block');
						}else if($scope.nowTemplate.histories[i].title == 'High boost'){
							$('.temDoubleRange'+$scope.nowTemplate.histories[i].order).css('display','block');
						}else{
							$('.temDoubleRange'+$scope.nowTemplate.histories[i].order).css('display','none');
						}
					}
				},1);
			}
		};
		
		$scope.newTemplate = function(){
			$('#template-bar').css('right','500px');
			$('#editer-bar').css('left','320px');
			setTimeout(function() {
				$('#template-bar').css('display','none');
				$('#filterbar').css('display', 'block');
				$('#editer-bar').css('display', 'none');
				$('#editerbar').css('display', 'block');
			}, 500);
			setTimeout(function() {
				$('#filterbar').css('right','0');
				$('#editerbar').css('left', '0');
			}, 1000);
		};
		
		$scope.goTemplate = function(){
			$('#filterbar').css('right','500px');
			$('#editerbar').css('left', '320px');
			setTimeout(function() {
				$('#template-bar').css('display','block');
				$('#filterbar').css('display', 'none');
				$('#editerbar').css('display', 'none');
			}, 500);
			setTimeout(function() {
				$('#template-bar').css('right','0');
			}, 1000);
			for(var i=0; i<$scope.templates.length; i++){
				$('.tem'+i+' #template-item').css('background-color','');
			}
		};
		
		$scope.temRangeUpdate = function(h,idx){
			var t_id = $scope.nowTemplate._id;
			var h_id = h._id;
			var input2;
			if($('.temDoubleRange'+idx).css('display') == 'none'){
				input2 = 0;
			}else{
				input2 = 1;
			}
			var updateElement = {
				title: h.title,
				min: h.min,
				max: h.max,
				step: h.step,
				value: h.value,
				value2: input2 ? h.value2 : 0,
				order: h.order
			};
		};
		
		$scope.temOrderUp = function(h, idx){
			var t_id = $scope.nowTemplate._id;
			var input2, order=h.order;
			if($('.temDoubleRange'+idx).css('display') == 'none'){
				input2 = 0;
			}else{
				input2 = 1;
			}
			if(h.order>0){
				countChange();
			}
			function countChange(){
				var his = $scope.nowTemplate.histories;
				for(var i=0; i<his.length; i++){
					if(his[i].order == order){
						var updateInfo = {
							title: his[i].title,
							min: his[i].min,
							max: his[i].max,
							step: his[i].step,
							value: his[i].value,
							value2: his[i].value2,
							order: order-1
						};
						Templates.updateHistory(t_id, his[i]._id, updateInfo).success(function(data){
							countChange2();
						});
					}
				}
			}
			function countChange2(){
				var his = $scope.nowTemplate.histories;
				for(var i=0; i<his.length; i++){
					if(his[i].order == order-1){
						var updateInfo2 = {
							title: his[i].title,
							min: his[i].min,
							max: his[i].max,
							step: his[i].step,
							value: his[i].value,
							value2: his[i].value2,
							order: order
						};
						Templates.updateHistory(t_id, his[i]._id, updateInfo2).success(function(data){
							$scope.templates = data;
							$scope.templateOpen($scope.idx);
							$scope.nowTemplate.histories = data[$scope.idx].histories;
						});
					}
				}
			}
		};
		$scope.temOrderDown = function(h, idx){
			var t_id = $scope.nowTemplate._id;
			var input2, order=h.order;
			if($('.temDoubleRange'+idx).css('display') == 'none'){
				input2 = 0;
			}else{
				input2 = 1;
			}
			if(h.order<$scope.nowTemplate.histories.length-1){
				countChange();
			}
			function countChange(){
				var his = $scope.nowTemplate.histories;
				for(var i=0; i<his.length; i++){
					if(his[i].order == order){
						var updateInfo = {
							title: his[i].title,
							min: his[i].min,
							max: his[i].max,
							step: his[i].step,
							value: his[i].value,
							value2: his[i].value2,
							order: order+1
						};
						Templates.updateHistory(t_id, his[i]._id, updateInfo).success(function(data){
							countChange2();
						});
					}
				}
			}
			function countChange2(){
				var his = $scope.nowTemplate.histories;
				for(var i=0; i<his.length; i++){
					if(his[i].order == order+1){
						var updateInfo2 = {
							title: his[i].title,
							min: his[i].min,
							max: his[i].max,
							step: his[i].step,
							value: his[i].value,
							value2: his[i].value2,
							order: order
						};
						Templates.updateHistory(t_id, his[i]._id, updateInfo2).success(function(data){
							$scope.templates = data;
							$scope.templateOpen($scope.idx);
							$scope.nowTemplate.histories = data[$scope.idx].histories;
						});
					}
				}
			}
		};
		
		$scope.filterOpen = function(cls){
			var height = height1;
			if(cls == 'bilateral'){
				$scope.filterDoubleRange = true;
				height = height2;
			}else if(cls == 'HB'){
				$scope.filterDoubleRange = true;
				height = height2;
			}else{
				$scope.filterDoubleRange = false;
			}
			for(var i=0; i<$scope.filters.length; i++){
				if(($('.'+cls+' .content').css('height'))=='0px'){
					for(var j=0; j<$scope.filters.length; j++){
						$('.content')[j].style.height = '0px';
					}
					for(var j=0; j<$scope.filters.length; j++){
						$('.'+$scope.filters[j].cls).removeClass('active');
					}
					$('.'+cls+' .content').css('height',height);
					$('.'+cls+'').addClass('active');
				}else{
					$('.'+cls+' .content').css('height','0px');
					$('.'+cls+'').removeClass('active');
				}
			}
		};
		
		$scope.filterAccept = function(cls, idx){
			var input = $('.'+cls+' input[type=range]#basic-range')[0];
			var input2 = $('.'+cls+' input[type=range]#double-range')[0];
			$scope.historyFormData = {
				title: $scope.filters[idx].title,
				min: input.min,
				max: input.max,
				step: input.step,
				value: input.value,
				value2: input2 ? input2.value : 0,
				order: $scope.histories.length
			};
			var compare=0;
			for(var i=0; i<$scope.histories.length; i++){
				if($scope.histories[i].title == $scope.historyFormData.title){
					compare++;
				}else{
				}
			}
			if(!compare){
				createHistory($scope.historyFormData);
			}
		};
		
		$scope.filterReset = function(cls, idx){
			$('.'+cls+' input[type=range]')[0].value = 0;
			$('.'+cls+' #rangeValue')[0].innerHTML = 0;
			$('.'+cls+' input[type=range]')[1].value = 0;
			$('.'+cls+' #range2Value')[0].innerHTML = 0;
		};
		
		$scope.showParameter = function(idx){
			$scope.editHistoryIdx = idx;
			$scope.editHistory = $scope.histories[idx];
			
			if($scope.editHistory.title=='Bilateral'){
				$scope.parameterDoubleRange = true;
			}else if($scope.editHistory.title=='High boost'){
				$scope.parameterDoubleRange = true;
			}else{
				$scope.parameterDoubleRange = false;
			}
		};
		
		$scope.historyOrderUp = function(){
			var idx = $scope.editHistoryIdx;
			var orderTemp = $scope.histories[idx].order;
			$scope.histories[idx].order = orderTemp-1;
			$scope.histories[idx-1].order = orderTemp;
		};
		$scope.historyOrderDown = function(){
			var idx = $scope.editHistoryIdx;
			var orderTemp = $scope.histories[idx].order;
			$scope.histories[idx].order = orderTemp+1;
			$scope.histories[idx+1].order = orderTemp;
		};
		$(document).keydown(function(evt){
			if(!$scope.nowTemplate)
				return false;
				
				var max = $scope.nowTemplate.histories.length;
				if(evt.keyCode == 32){
					if($scope.spaceCnt==0){
						for(var i=0; i<max; i++){
							$('.applyCheck'+i).css('display','none');
						}
					}else if($scope.spaceCnt>max){
						$scope.spaceCnt=0;
						for(var i=0; i<max; i++){
							$('.applyCheck'+i).css('display','none');
						}
					}
					for(var i=0; i<$scope.spaceCnt; i++){
						$('.applyCheck'+i).css('display','inline-block');	
					}
					$scope.spaceCnt++;
					return false;
				}
		});
		
		var canvas = document.getElementById('imageFilterCanvas');
		var files = document.getElementById('files');
		var ctx = canvas.getContext('2d');
		var canvas2 = document.getElementById('imageCanvas');
		
		function selectImage(e){
			var input = e.target;
			
			inputImage(input);
		}
		
		document.getElementById('input-file').addEventListener('change', selectImage, false);
		
		function handleDragOver(e){
            e.stopPropagation();
            e.preventDefault();
		}
		
		function handleDrop(e){
            e.stopPropagation();
            e.preventDefault();
			var input = e.dataTransfer;
			
			inputImage(input);
		}
		
		canvas.addEventListener('dragover', handleDragOver, false);
		canvas.addEventListener('drop', handleDrop, false);
		files.addEventListener('dragover', handleDragOver, false);
		files.addEventListener('drop', handleDrop, false);
		
		function inputImage(input){
			$scope.imageSaveName = input.files[0].name;
			if(input.files && input.files[0]){
				var reader = new FileReader();
				reader.onload = function(e){
					$('#imgView').attr('src', e.target.result);
					var img = $('#imgView')[0];
					var imgCheck = e.target.result.split('/');
					if(imgCheck[0] == 'data:image'){
						$('#downloadLnk').css('display','block');
					}
					else{
						$('#downloadLnk').css('display','none');
					}
					img.onload = function(e){
						canvas2.height = img.height;
						canvas2.width = img.width;
						var ctx2 = canvas2.getContext('2d');
						ctx.clearRect(0,0, canvas.width, canvas.height);
						ctx2.clearRect(0,0, canvas.width, canvas.height);
						ctx2.drawImage(img, 0, 0);
						$('#imageFilterCanvas').css('display', 'block');
						fitImageOn(canvas, img);
					}
					$('#files').css('display', 'none');
				};
				reader.readAsDataURL(input.files[0]);
			}
		}
		function fitImageOn(canvas, imageObj){
			var imageAspectRatio = imageObj.width / imageObj.height;
			var canvasAspectRatio = canvas.width / canvas.height;
			var renderableHeight, renderableWidth, xStart, yStart;
		
			if(imageObj.width<canvas.width && imageObj.height<canvas.height){
				renderableHeight = imageObj.height;
				renderableWidth = imageObj.width;
				xStart = (canvas.width-imageObj.width) / 2;
				yStart = (canvas.height - imageObj.height) / 2;
			}
			else if(imageAspectRatio < canvasAspectRatio) {
				renderableHeight = canvas.height;
				renderableWidth = imageObj.width * (renderableHeight / imageObj.height);
				xStart = (canvas.width - renderableWidth) / 2;
				yStart = 0;
			}
			else if(imageAspectRatio > canvasAspectRatio) {
				renderableWidth = canvas.width;
				renderableHeight = imageObj.height * (renderableWidth / imageObj.width);
				xStart = 0;
				yStart = (canvas.height - renderableHeight) / 2;
			}
			else {
				renderableHeight = canvas.height;
				renderableWidth = canvas.width;
				xStart = 0;
				yStart = 0;
			}
			ctx.drawImage(imageObj, xStart, yStart, renderableWidth, renderableHeight);
		}
		
		//save image
		var downloadLnk = document.getElementById('downloadLnk');
		downloadLnk.addEventListener('click', function(ev){
			var imageSaveName = ($scope.imageSaveName).substring(0, $scope.imageSaveName.length-4);
			console.log(imageSaveName);
			downloadLnk.href = canvas2.toDataURL();
			downloadLnk.download = imageSaveName+'_cybermed';
		}, false);
		
		$scope.valueChange = function(m, v){
			var val = (v*100) / m;
			var value = val - (val*0.09);
			$('span#pips').css('left', value+'%');
		};		
	})
	.controller('createTemplateCtrl', function ($scope, $modalInstance, Templates, histories) {		
	    $scope.modalCancel = function(){
            $modalInstance.close();
		};
		
		$scope.createTemplate = function(){
			if($scope.templateName){
				$scope.data = {
					templateName: $scope.templateName
				};
				Templates.create($scope.data)
				.success(function (t){
					var template = t[t.length-1];
					for(var i=0; i<histories.length; i++){
						Templates.addHistory(template._id, {
							title: histories[i].title,
							min: histories[i].min,
							max: histories[i].max,
							step: histories[i].step,
							value: histories[i].value,
							value2: histories[i].value2 ? histories[i].value2 : 0,
							order: histories[i].order
						}).success(function (data){
							$modalInstance.close(t);
						});
					}
				});
			}else{
				$('#enterTemplate-helper')[0].style.display = 'block';
			}
		};
	})
	.controller('deleteCtrl', function ($scope, $modalInstance) {		
	    $scope.cancel = function(){
            $modalInstance.close();
		};
		
		$scope.delete = function(){
			$modalInstance.close(true);
		};
	});;