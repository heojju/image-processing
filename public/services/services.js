angular.module('Service', [])
    .factory('Histories', ['$http',
        function ($http) {
            return {
                get: function () {
                    return $http.get('/api/histories');
                },
                select: function (id) {
                    return $http.get('/api/histories/' + id);
                },
                create: function (info) {
                    return $http.post('/api/histories', info);
                },
                delete: function (id) {
                    return $http.delete('/api/histories/' + id);
                },
                update: function (id, element) {
                    return $http.post('/api/histories/' + id, element);
                }
            };
        }
    ])
    .factory('Templates', ['$http',
        function ($http) {
            return {
                get: function () {
                    return $http.get('/api/templates');
                },
                select: function (id) {
                    return $http.get('/api/templates/' + id);
                },
                create: function (info) {
                    return $http.post('/api/templates', info);
                },
                delete: function (id) {
                    return $http.delete('/api/templates/' + id);
                },
                update: function (id, element) {
                    return $http.post('/api/templates/' + id, element);
                },
                addHistory: function(id, history){
                    return $http.post('/api/templates/add/history/' + id, history);
                },
                removeHistory: function(id, idx){
                    return $http.post('/api/templates/delete/history/' + id, {
                        idx: idx
                    });
                },
                updateHistory: function(id, idx, task){
                    return $http.post('/api/templates/update/history/'+ id, {
                        idx: idx,
                        task: task
                    });
                }
                // 추후진행예정(removeHistory)
            };
        }
    ]);