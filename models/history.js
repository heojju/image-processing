var mongoose = require('mongoose');

var HistorySchema = {
	title:{
		type: String
	},
	min:{
		type: Number,
		default: 0
	},
	max:{
		type: Number,
		default: 1
	},
	step:{
		type: Number,
		default: 0.1
	},
	value:{
		type: Number
	},
	value2:{
		type: Number
	},
	order:{
		type: Number
	}
};

module.exports = mongoose.model('history', HistorySchema);