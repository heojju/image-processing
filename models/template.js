var mongoose = require('mongoose');
var historySchema = require('./history.js');

var TemplateSchema = {
	templateName:{
		type: String
	},
	histories: [historySchema.schema]
};

module.exports = mongoose.model('template', TemplateSchema);